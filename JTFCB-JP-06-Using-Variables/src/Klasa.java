
public class Klasa {

	public static void main(String[] args) {
		int myNumber = 88;
		// mozna te� inicjalizowa� oddzielnie w dwoch krokach
		// int myNumber;
		// myNumber = 88;
		
		short myShort = 847;
		long myLong = 9797;
		double myDouble = 7.3243;
		
		// ten f sie dodaje zeby zmienic na float na serio
		// default jest double dla przecinkowej nawet jak pisze float!!!
		float myFloat = 324.3f;
		char myChar = 'A';
		boolean myBoolean = false;
		byte myByte = 127;
		
		
		System.out.println(myNumber);
		System.out.println(myShort);
		System.out.println(myLong);
		System.out.println(myDouble);
		System.out.println(myFloat);
		System.out.println(myChar);
		System.out.println(myBoolean);
		System.out.println(myByte);
	}

}
